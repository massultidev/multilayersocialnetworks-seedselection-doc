\section{Wielowarstwowe sieci społeczne}

Pomimo tego że procesy propagacyjne w sieciach społecznych zostały
już gruntownie przebadane w ciągu ostatniej dekady \cite{ref1},
to jednak badania te były prowadzane dla jednowarstwowych sieci społecznych.
Natomiast prawdziwa esencja zjawiska propagacji w sieciach społecznych
rzadko kiedy może być zaobserwowana w sieciach jednowarstwowych \cite{ref7}.
(Sieci jednowarstwowe nazywamy
\textbf{sieciami monopleksowymi – ang. monoplex networks}.)

Wyraźnie można to zauważyć na przykładzie internetowego rozprzestrzeniania
się informacji, gdzie to proces przechodzenia między sieciami
podczas dzielenia się informacjami stał się jedną z podstawowych
funkcjonalności.
Jako inny przykład można natomiast podać zarażonego
człowieka podróżującego różnymi formami komunikacji \cite{ref1}.

Pojęcie wielowarstwowych sieci społecznych to tak naprawdę termin zbiorowy
określający wiele typów sieci składających się z więcej niż jednej warstwy
\cite{ref1,ref6}.
Zaliczamy tu miedzy innymi:
\begin{itemize}
	\item interconnected networks,
	\item multiplex networks,
	\item multivariate network,
	\item hypernetwork,
	\item coupled network,
	\item network of networks,
	\item meta-matrix,
	\item ... (i jeszcze wiele innych).
\end{itemize}

Jednakże \textbf{wielowarstwową sieć społeczną} - najczęściej cechuje
fakt iż składa się ona z kilku klasycznych sieci społecznych.
Z których każda stanowi osobną warstwę.
W sieciach o wielu warstwach mogą lub nie występować tzw. inter-połączenia.
Czyli połączenia między węzłami znajdującymi się na różnych warstwach.
Natomiast każdy węzeł występujący na jednej z warstw
może (i zwykle ma) swojego odpowiednika w pozostałych warstwach.

\subsection{Różnice w mechanizmie propagacji}

Pod względem rozłożenia węzłów rozróżniamy kilka rodzajów wielowarstwowych
sieci społecznych.
W jednym ze skrajnych przypadków wszystkie warstwy sieci zawierają
te same węzły.
Istnieje również odwrotna skrajność w której to każdy z węzłów
występuje tylko w pojedynczej warstwie \cite{ref1}.

Oczywiście ten pierwszy przypadek ma znamiona zbyt idealnego wzorca,
pozbawionego naturalności.
Natomiast ten drugi wyklucza możliwość propagacji między warstwami.
Dlatego najlepszym wydaje się być model w którym to niektóre
węzły występują tylko na pojedynczej warstwie, inne w kilku z nich,
a niektóre nawet na wszystkich.

\begin{figure}[H]
	\centering
	\makebox[\textwidth]{\includegraphics[width=\textwidth]
		{content/data/image02.png}}
	\caption{Rysunek przedstawia mechanizmy rozchodzennia się
		kaskad wpływów w wielowarstwowych sieciach społecznych \cite{ref1}.}
	\label{image02}
\end{figure}

Na rysunku \ref{image02} z wizualizowana została wielowarstwowa
sieć społeczna charakteryzująca się opisaną wcześniej zróżnicowaną budową.

Jak widać na przykładzie krawędzi
(v\textsubscript{4}, l\textsubscript{2}),
(v\textsubscript{4}, l\textsubscript{3}) i
(v\textsubscript{5}, l\textsubscript{2})
w sieciach tego typu wiele warstw i wiele krawędzi może mieć
ze sobą połączenia.

Przy założeniu że L/l definiują warstwę (ang. layer),
a v krawędź (ang. verge).

\bigskip

Opierając się na tym podstawowym modelu możemy dodać dodatkowe atrybuty.
Takie jak chociażby tymczasowy wymiar opisujący konkretny krok czasowy.

Wszystko po to aby odróżnić stan węzła takiego jak chociażby
\textit{v}\textit{\textsubscript{4}} na warstwie
\textit{l}\textit{\textsubscript{2}} w czasie.

Zaproponowana notacja przedstawiona została w tabeli \ref{table-notation}.

\begin{figure}[H]
\begin{table}[H]
\centering
\caption{Notacja zapożyczona z artykułu \cite{ref1}.}
\label{table-notation}
\begin{tabular}{l|l}
\textbf{V}                      & Zbiór węzłów w wielowarstwowej sieci.                                                                                                                   \\
\textbf{L}                      & Zbiór warstw w wielowarstwowej sieci.                                                                                                                   \\
\textbf{n}                      & Liczba węzłów w wielowarstwowej sieci.                                                                                                                   \\
\textbf{$(u, l_u)$}             & Odpowiada węzłowi u w warstwie $l_u$ w wielowarstwowej sieci.                                                                                            \\
\textbf{$((u, l_u), (v, l_v))$} & \begin{tabular}[c]{@{}l@{}}Krotka reprezentująca krawędź między węzłami\\ u i v odpowiednio na warstwach  $l_u i l_v$.\end{tabular}                      \\
\textbf{C}                      & Kaskada propagacji.                                                                                                                                      \\
\textbf{$(u,l_u,v,l_v,t)_c$}    & \begin{tabular}[c]{@{}l@{}}Krotka opisująca krokową propagację między dwoma węzłami\\ (u w warstwie $l_u$ i v w warstwie $l_v$) w chwili t.\end{tabular} \\
\textbf{D}                      & Sieć propagacji w wielowarstwowej sieci.                                                                                                                
\end{tabular}
\end{table}
\end{figure}

Rysunki \ref{image02}b i \ref{image02}c przedstawiają dwie kaskady
wpływów powstałe w naszym
teoretycznym modelu wielowarstwowej sieci społecznej po zarażeniu
odpowiednio węzłów
\textit{v}\textit{\textsubscript{4}} w warstwie
\textit{l}\textit{\textsubscript{2}} oraz
\textit{v}\textit{\textsubscript{4}} w warstwie
\textit{l}\textit{\textsubscript{1}}.

\bigskip

Natomiast rysunek \ref{image02}d wizualizuje agregację obu wspomnianych
kaskad równocześnie będąc wizualizacją
jej sieci propagacji przedstawioną na tle pełnej wielowarstwowej sieci.

\bigskip

Jedną z najważniejszych cech procesów propagacyjnych w wielowarstwowych
sieciach społecznych jest fakt że byty mogą być
przenoszone między warstwami.

W sumie istnieją cztery sposoby w jakie byt może przemierzać wielowarstwową
sieć społeczną \cite{ref1}:

\begin{itemize}
	\item \textbf{same-node inter-layer} – Wtedy gdy kaskada przechodzi
		do innej warstwy ale zostaje w tym samym węźle.
		Przykład: Autor post'u na Facebook'u udostępnia go na Twitter'rze.
	\item \textbf{other-node inter-layer} – Wtedy gdy kaskada przechodzi
		do innej warstwy i innego węzła.
		Przykład: Użytkownik wysyła email'a do innego użytkownika
		korzystającego z innego serwisu mail'owego.
	\item \textbf{other-node intra-layer} – Wtedy gdy kaskada kontynuuje
		rozprzestrzenianie na tej samej warstwie.
		Przykład: Użytkownik retweet'uje cudzy post na Twitter'rze.
	\item \textbf{same-node-intra-layer} – Generalnie nie uważa się
		tego zjawiska za istotne i najczęściej pomija
		się je w badaniach na temat procesów propagacyjnych
		w sieciach społecznych.
\end{itemize}

\bigskip

Podsumowanie wspomnianych sposobów znaleźć możemy na rysunku \ref{image03}.

\bigskip

\begin{figure}[H]
	\centering
	\makebox[\textwidth]{\includegraphics[width=\textwidth]{content/data/image03.png}}
	\caption{Dostępne możliwości rozprzestrzeniania się bytu
		w wielowarstwowej sieci społecznej \cite{ref1}.}
	\label{image03}
\end{figure}

Warto również zwrócić uwagę na to czym różnią się modele propagacyjne
takie jak LTM czy ICM w środowisku wielowarstwowym
od klasycznego.
Istnieje kilka podejść, jednakże wzorując się na
\cite{ref30,ref31,ref32,ref33,ref34,ref35}
praca ta przyjmuje jedną bardzo istotną modyfikację.

Tak naprawdę różnica jest bardzo drobna, ale jakże istotna.
Jeżeli w danym kroku \textit{t} zainfekowany zostanie węzeł
$(u, l_u)$, to w kroku \textit{t+1} zainfekowany również
będzie węzeł $(u, l_v)$.
Czyli jego odpowiednik na warstwie $l_v$.
Przy czym zakładamy tutaj że mamy tylko dwie warstwy $l_u$ i $l_v$.
W sytuacji występowania większej ilości warstw
sytuacja jest analogiczna dla każdej następnej warstwy.
Przykład:
Załóżmy że istnieje jeszcze warstwa $l_z$ na której również znajduje
się odpowiednik $(u, l_u)$, czyli $(u, l_z)$.
On również zostanie zainfekowany w kroku \textit{t+1}.

\pagebreak

\subsection{Nowe wyzwania}

Śmiałe posunięcie jakim było wprowadzenie koncepcji wielowarstwowych sieci
społecznych otworzyło przed badaczami możliwości konstruowania modeli
lepiej oddających złożoną naturę procesów propagacyjnych.
W efekcie dając nadzieje na przygotowanie lepszych algorytmów.
Pozwalających zarówno na dokładniejsze przewidywanie występowania
konkretnych zjawisk, jak i efektywniejsze ich wywoływanie.

Jednakże wraz z wprowadzenie konceptu wielowarstwowych sieci społecznych
pojawiło się sporo nowych problemów \cite{ref1}.

Przede wszystkim okazało się, że większość wykorzystywanych wcześniej
algorytmów w najlepszym wypadku wymagała rozszerzenia aby nadawać
się do wykorzystania w nowych warunkach.
Natomiast w najgorszym okazało się, że z niektórych algorytmów
nie da się zrobić w nowych warunkach żadnego użytku \cite{ref1}
(np. algorytm wyboru węzłów początkowych k-Shell).

Pojawiły się również kompletnie nowe wyzwania.
Takie jak chociażby uwzględnienie i sensowne modelowanie występowania
kosztu przejścia między warstwami
\textbf{(ang. layer-switching/layer-crossing cost/overhead)}.
Rozumiany fizycznie jako chociażby przejście z przystanku metra
na przystanek autobusowy, albo w innym wypadku opublikowanie
tego samego post'u z Facebook'a na Twitter'rze \cite{ref1}.

W obecnej chwili wiele algorytmów, czy modeli propagacyjnych zostało
już zaadaptowanych do użycia w środowisku wielowarstwowych sieci społecznych.
Mimo to badania nad tą dziedziną są nadal we wczesnej
i mocno rozwijającej się fazie.

